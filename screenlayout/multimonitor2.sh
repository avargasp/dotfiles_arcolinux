#!/bin/sh
xrandr \
  --output HDMI-0 --off \
  --output DP-0 --off \
  --output DP-1 --off \
  --output DP-2 --mode 2560x1440 --pos 2560x0 --rotate left \
  --output DP-3 --off \
  --output DP-4 --mode 2560x1440 --rate 144 --pos 0x0 --rotate normal --primary \
  --output DP-5 --off \
  --dpi 96
