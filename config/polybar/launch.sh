#!/usr/bin/env sh

# More info : https://github.com/jaagr/polybar/wiki

# Install the following applications for polybar and icons in polybar if you are on ArcoLinuxD
# awesome-terminal-fonts
# Tip : There are other interesting fonts that provide icons like nerd-fonts-complete
# --log=error
# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

count=$(xrandr --query | grep " connected" | cut -d" " -f1 | wc -l)

if [[ $count -eq 1 ]]
then 
  PRIMARY=$(xrandr --query | grep " connected" | cut -d" " -f1)
else
  MULTIPLE=true
  PRIMARY=$(xrandr --query | grep " connected" | grep "primary" | cut -d" " -f1)
  OTHERS_HORIZONTAL=$(xrandr --query | grep " connected" | grep -v "primary" | egrep -v ' left \(| right \(' | cut -d" " -f1)
  OTHERS_VERTICAL=$(xrandr --query | grep " connected" | grep -v "primary" | egrep ' left \(| right \(' | cut -d" " -f1)
fi

if type "xrandr" > /dev/null; then
  # Launch on primary monitor
  MONITOR=$PRIMARY polybar --reload mainbar -c ~/.config/polybar/config &
  
  if [[ $MULTIPLE ]]
  then
    # Launch on all other monitors
    for m in $OTHERS_HORIZONTAL; do
      MONITOR=$m polybar --reload secondarybar -c ~/.config/polybar/config &
    done
    for m in $OTHERS_VERTICAL; do
      MONITOR=$m polybar --reload verticalbar -c ~/.config/polybar/config &
    done
  fi
fi